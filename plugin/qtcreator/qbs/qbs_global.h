#ifndef QBS_GLOBAL_H
#define QBS_GLOBAL_H

#if defined(QBS_LIBRARY)
#  define QBS_EXPORT Q_DECL_EXPORT
#else
#  define QBS_EXPORT Q_DECL_IMPORT
#endif

#endif // QBS_GLOBAL_H
