#ifndef QBSCONSTANTS_H
#define QBSCONSTANTS_H

namespace QBS {
namespace Constants {

const char ACTION_ID12[] = "QBS.Action12";
const char MENU_ID12[] = "QBS.Menu12";

const char ACTION_ID8[] = "QBS.Action8";
const char MENU_ID8[] = "QBS.Menu8";

const char ACTION_ID11[] = "QBS.Action11";
const char MENU_ID11[] = "QBS.Menu11";



QString part_1 = "<cmd-end/>\n\
        <cmd-start/>\n\
        \n\
        <set-target-canvas>\n\
        <canvas-filename>CURRENT-CANVAS</canvas-filename>\n\
        </set-target-canvas>\n\
        <网络文件独占模式>\n\
         <action--drop-package \n\
                       parent-uid = \"FIRST-CHOOSED\" \n\
                               x=\"500\" y =\"400\"  \n\
                       XY-确定方法 = \"FIRST-CHOOSED > RIGHT-TOP > 0 > 0\"\n\
                               flags = \"x-y-is-canvas-coordinate\"\n\
                               filename=\"./package/000/pkg/src-location.better.pkg\" start-pos=\"48\" \n\
                               />\n\
        <sequence-operation-on-package>\n\
             <package-uid>CREATED-JUST-NOW</package-uid> \n\
        \n\
        <port-index> 4  </port-index>   <port-value>";

QString part_2 = "</port-value> <port-index> 5  </port-index>   <port-value>";
QString part_3 = "</port-value>";
QString part_4 = "<package-uid>FIRST-CHOOSED</package-uid>\n\
        <get-package-or-jci-include-self/>\n\
        <发送canvas定位xml命令>\n\
              <文件类型>";

QString part_5 = "</文件类型>";
QString part_6 = "</发送canvas定位xml命令>\n\
        </sequence-operation-on-package >\n\
        </网络文件独占模式>\n\
        <cmd-end/>   ";




} // namespace Constants

QString host_address = "127.0.0.1";
QString host_port    = "27193";
int host_port_int    =  27193;


enum qbs_file_type {
    FILE_TYPE__UNKNOWN=  0,
    FILE_TYPE__C_SERIES ,
    FILE_TYPE__SHELL,
    FILE_TYPE__ELISP ,
    FILE_TYPE__PYTHON ,
};


QString part_a = "[";
QString part_b = "]";

QString relative_path_1 = "/home/guest/qbs/";
QString relative_path_2 = "~/qbs/";

int relative_path_1_len = 0;
int relative_path_2_len = 0;


} // namespace QBS

#endif // QBSCONSTANTS_H
