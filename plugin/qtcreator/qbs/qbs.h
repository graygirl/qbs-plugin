#ifndef QBSPLUGIN_H
#define QBSPLUGIN_H

#include "qbs_global.h"

#include <extensionsystem/iplugin.h>
//#include <qnetworkinterface.h>
#include <QTcpSocket>

namespace Qbs {
namespace Internal {

class QbsPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "Qbs.json")

public:
    QbsPlugin();
    ~QbsPlugin() override;

    bool initialize(const QStringList &arguments, QString *errorString) override;
    void extensionsInitialized() override;
    ShutdownFlag aboutToShutdown() override;

    QString getRandomString(int length);
    QStringList file_postfix_list;
    int getFiletypeByPostfix(const QString postfix);
    void setStringPart_a_and_b_according_filetype(int filetype);
    QString getRelativePathFilename(QString fullpath_filename);

    QTcpSocket * client = nullptr;
    int client_connect();

    void readData__client();
    void writeData__client(QString data);
private:
    void triggerAction12();
    void triggerAction8();
    void triggerAction11();
};

} // namespace Internal
} // namespace Qbs

#endif // QBSPLUGIN_H
