# vscode plugin for qbs.

## Features
   jump around between source file and flow chart. 
## Requirements
    1. 安装qbs,并可以正常运行
    2. 打开 ~/.bashrc  (如果这个文件不存在，就通过命令： >~/.bashrc 来创建一个)
       添加 export QBS_INSTALL_PATH_1="/home/xxx/.../"  (xxx 代表用户名 ... 代表安装目录)
            export QBS_INSTALL_PATH_2="~/.../"

## 安装
   install from VSIX ：qbs-0.0.1.vsix
    
## use
    #######################################################################################
    使用方法：(qbs处于打开状态)
    注意：只有在qbs安装目录下的文件才有效。目前遵循的原则是qbs只会去读写qbs目录下的文件

                 1. make sure the communication channel is open, 一般qbs运行状态时就是打开的。
          <F8> : 2. choose one item in canvas
                 3. press <F8> in 编辑器, 在选中的 canvas item 旁边会自动生成这样的一个包, 同时编辑器中会自动添加一行文本

          <F10>: 向上搜索 （%%    %%）， 并发送到qbs, 从而实现跳转到流程图      

          <F12>: 向下搜索 （%%    %%）， 并发送到qbs, 从而实现跳转到流程图

#######################################################################################

## Release Notes
### 1.0.0
## Following extension guidelines


**Enjoy!**
